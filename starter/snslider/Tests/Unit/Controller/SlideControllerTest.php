<?php
namespace Sesamnet\Snslider\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Joel Frehner <joel.frehner@sesamnet.ch>
 */
class SlideControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Sesamnet\Snslider\Controller\SlideController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Sesamnet\Snslider\Controller\SlideController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllSlidesFromRepositoryAndAssignsThemToView()
    {

        $allSlides = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $slideRepository = $this->getMockBuilder(\Sesamnet\Snslider\Domain\Repository\SlideRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $slideRepository->expects(self::once())->method('findAll')->will(self::returnValue($allSlides));
        $this->inject($this->subject, 'slideRepository', $slideRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('slides', $allSlides);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }
}
