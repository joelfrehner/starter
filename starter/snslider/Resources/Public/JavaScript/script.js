$(document).ready(function(){

    var slider = $('.slider');
    if (slider.length > 0) {
        slider.slick({
            slidesToShow: slidesToShow,
            slidesToScroll: slidesToScroll,
            autoplay: autoplay,
            autoplaySpeed: autoplaySpeed,
            arrows: showArrows,
            dots: showDots,
            infiniteLoop: infiniteLoop,
            zIndex: 50,
        });
    }

});