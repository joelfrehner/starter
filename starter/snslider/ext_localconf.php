<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function($extKey)
    {

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Sesamnet.Snslider',
        'Slideon',
        [
            'Slide' => 'list'
        ],
        // non-cacheable actions
        [
            'Slide' => 'list'
        ]
    );

	// wizards
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
		'mod {
			wizards.newContentElement.wizardItems.plugins {
				elements {
					slideon {
						icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($extKey) . 'Resources/Public/Icons/user_plugin_slideon.svg
						title = LLL:EXT:snslider/Resources/Private/Language/locallang_db.xlf:tx_snslider_domain_model_slideon
						description = LLL:EXT:snslider/Resources/Private/Language/locallang_db.xlf:tx_snslider_domain_model_slideon.description
						tt_content_defValues {
							CType = list
							list_type = snslider_slideon
						}
					}
				}
				show = *
			}
	   }'
	);
    },
    $_EXTKEY
);