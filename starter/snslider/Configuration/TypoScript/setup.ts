/*
plugin.tx_snslider_slideon {
  view {
    templateRootPaths.0 = EXT:snslider/Resources/Private/Templates/
    templateRootPaths.1 = plugin.tx_snslider_slideon.view.templateRootPath
    partialRootPaths.0 = EXT:snslider/Resources/Private/Partials/
    partialRootPaths.1 = plugin.tx_snslider_slideon.view.partialRootPath
    layoutRootPaths.0 = EXT:snslider/Resources/Private/Layouts/
    layoutRootPaths.1 = plugin.tx_snslider_slideon.view.layoutRootPath
  }
  persistence {
    storagePid = plugin.tx_snslider_slideon.persistence.storagePid
    #recursive = 1
  }
  features {
    #skipDefaultArguments = 1
  }
  mvc {
    #callDefaultActionIfActionCantBeResolved = 1
  }
}

plugin.tx_snslider._CSS_DEFAULT_STYLE (
    textarea.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    input.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    .tx-snslider table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-snslider table th {
        font-weight:bold;
    }

    .tx-snslider table td {
        vertical-align:top;
    }

    .typo3-messages .message-error {
        color:red;
    }

    .typo3-messages .message-ok {
        color:green;
    }
)
*/
page.includeCSS.slick = https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css
page.includeCSS.slider = EXT:snslider/Resources/Public/Css/Slider.css
#page.includeJSFooter.jquery = https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js
page.includeJSFooter.slick = https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.js
page.includeJSFooter.main = EXT:snslider/Resources/Public/JavaScript/script.js