<?php
namespace Sesamnet\Snslider\Domain\Repository;

/***
 *
 * This file is part of the "Sesamnet Slider" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Joel Frehner &lt;joel.frehner@sesamnet.ch&gt;, Sesamnet GmbH
 *
 ***/

/**
 * The repository for Slides
 */
class SlideRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * Find slides by selection
     * @param string $selected commaseparated list of selected slides
     * @return array $slides
     */
    public function findSelected($selected)
    {
        $vars = str_replace("tx_snslider_domain_model_slide_", "", $selected);
        $selected = explode(",", $vars);
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->matching($query->in('uid', $selected));
        return $query->execute();
    }

}
