<?php
namespace Sesamnet\Snslider\Controller;

/***
 *
 * This file is part of the "Sesamnet Slider" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Joel Frehner &lt;joel.frehner@sesamnet.ch&gt;, Sesamnet GmbH
 *
 ***/

/**
 * SlideController
 */
class SlideController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * slideRepository
     *
     * @var \Sesamnet\Snslider\Domain\Repository\SlideRepository
     * @inject
     */
    protected $slideRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {

        $mode = $this->settings['mode'];

        if ($mode == 'headerimage') {
            $slides = $this->slideRepository->findSelected($this->settings['singleselect']);
        } else if ($mode == 'sliderselect') {
            $slides = $this->slideRepository->findSelected($this->settings['multiselect']);
            if ($slides->count() == 0) {
                $slides = $this->slideRepository->findAll();
            }
        } else {
            $slides = $this->slideRepository->findAll();
        }

        $this->view->assign('slides', $slides);
        $this->view->assign('mode', $mode);
    }
}