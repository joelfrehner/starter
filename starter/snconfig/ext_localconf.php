<?php
// ============================================== //
//                                           _
//                                          | |
//  ___  ___  ___  __ _ _ __ ___  _ __   ___| |_
// / __|/ _ \/ __|/ _` | '_ ` _ \| '_ \ / _ \ __|
// \__ \  __/\__ \ (_| | | | | | | | | |  __/ |_
// |___/\___||___/\__,_|_| |_| |_|_| |_|\___|\__|
//
// Project: 	SNBASE
// Version: 	1.0.0
// Author:		Joel Frehner
// Year:			© 2018
//
// ============================================== //



if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

// ========================== //
// PAGE TS
// ========================== //

// TCEMAIN
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/PageTS/TCEMAIN.ts">');

// TCEFORM
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/PageTS/TCEFORM.ts">');

// RTE
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/PageTS/RTE.ts">');

// USER
//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/PageTS/USER.txt">');

// CUSTOM RTE
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['custom_rte'] = 'EXT:snconfig/Configuration/PageTS/RTE/Custom_RTE.yaml';