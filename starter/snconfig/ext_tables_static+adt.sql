--
-- Tabellenstruktur für Tabelle sys_language
--
DROP TABLE IF EXISTS sys_language;
CREATE TABLE sys_language (
  uid int(11) unsigned NOT NULL AUTO_INCREMENT,
  pid int(11) unsigned NOT NULL DEFAULT '0',
  tstamp int(11) unsigned NOT NULL DEFAULT '0',
  hidden tinyint(4) unsigned NOT NULL DEFAULT '0',
  title varchar(80) NOT NULL DEFAULT '',
  flag varchar(20) NOT NULL DEFAULT '',
  language_isocode varchar(2) NOT NULL DEFAULT '',
  static_lang_isocode int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (uid),
  KEY parent (pid)
);

INSERT INTO sys_language VALUES
(1, 0, 1490004900, 1, 'Français', 'fr', 'fr', 0),
(2, 0, 1490004902, 1, 'English', 'en-us-gb', 'en', 0);

--
-- Tabellenstruktur für Tabelle sys_file_storage
--
DROP TABLE IF EXISTS sys_file_storage;
CREATE TABLE sys_file_storage (
  uid int(11) NOT NULL AUTO_INCREMENT,
  pid int(11) NOT NULL DEFAULT '0',
  tstamp int(11) NOT NULL DEFAULT '0',
  crdate int(11) NOT NULL DEFAULT '0',
  cruser_id int(11) NOT NULL DEFAULT '0',
  deleted tinyint(4) NOT NULL DEFAULT '0',
  name varchar(30) NOT NULL DEFAULT '',
  description text,
  driver tinytext,
  configuration text,
  is_default tinyint(4) NOT NULL DEFAULT '0',
  is_browsable tinyint(4) NOT NULL DEFAULT '0',
  is_public tinyint(4) NOT NULL DEFAULT '0',
  is_writable tinyint(4) NOT NULL DEFAULT '0',
  is_online tinyint(4) NOT NULL DEFAULT '1',
  auto_extract_metadata tinyint(4) NOT NULL DEFAULT '1',
  processingfolder tinytext,
  PRIMARY KEY (uid),
  KEY parent (pid,deleted)
);

INSERT INTO sys_file_storage VALUES
(1, 0, 1489572407, 1489572407, 0, 0, 'fileadmin/ (auto-created)', 'This is the local fileadmin/ directory. This storage mount has been created automatically by TYPO3.', 'Local', '<?xml version="1.0" encoding="utf-8" standalone="yes" ?>\n<T3FlexForms>\n    <data>\n        <sheet index="sDEF">\n            <language index="lDEF">\n                <field index="basePath">\n                    <value index="vDEF">fileadmin/</value>\n                </field>\n                <field index="pathType">\n                    <value index="vDEF">relative</value>\n                </field>\n                <field index="caseSensitive">\n                    <value index="vDEF">1</value>\n                </field>\n            </language>\n        </sheet>\n    </data>\n</T3FlexForms>', 1, 1, 1, 1, 1, 1, NULL);



--
-- Tabellenstruktur für Tabelle sys_filemounts
--
DROP TABLE IF EXISTS sys_filemounts;
CREATE TABLE sys_filemounts (
  uid int(11) unsigned NOT NULL AUTO_INCREMENT,
  pid int(11) unsigned NOT NULL DEFAULT '0',
  tstamp int(11) unsigned NOT NULL DEFAULT '0',
  title varchar(30) NOT NULL DEFAULT '',
  description varchar(2000) NOT NULL DEFAULT '',
  path varchar(120) NOT NULL DEFAULT '',
  base int(11) unsigned NOT NULL DEFAULT '0',
  hidden tinyint(3) unsigned NOT NULL DEFAULT '0',
  deleted tinyint(1) unsigned NOT NULL DEFAULT '0',
  sorting int(11) unsigned NOT NULL DEFAULT '0',
  read_only tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (uid),
  KEY parent (pid)
);

INSERT INTO sys_filemounts VALUES
(1, 0, 1490111394, 'Bilder', '', '/bilder/', 1, 0, 0, 256, 0),
(2, 0, 1490111415, 'Dokumente', '', '/dokumente/', 1, 0, 0, 128, 0),
(3, 0, 1490111435, 'Videos', '', '/video/', 1, 0, 0, 64, 0);



--
-- Tabellenstruktur für Tabelle be_groups
--
DROP TABLE IF EXISTS be_groups;
CREATE TABLE be_groups (
  uid int(11) unsigned NOT NULL AUTO_INCREMENT,
  pid int(11) unsigned NOT NULL DEFAULT '0',
  tstamp int(11) unsigned NOT NULL DEFAULT '0',
  title varchar(50) NOT NULL DEFAULT '',
  non_exclude_fields text,
  explicit_allowdeny text,
  allowed_languages varchar(255) NOT NULL DEFAULT '',
  custom_options text,
  db_mountpoints text,
  pagetypes_select varchar(255) NOT NULL DEFAULT '',
  tables_select text,
  tables_modify text,
  crdate int(11) unsigned NOT NULL DEFAULT '0',
  cruser_id int(11) unsigned NOT NULL DEFAULT '0',
  groupMods text,
  file_mountpoints text,
  file_permissions text,
  hidden tinyint(1) unsigned NOT NULL DEFAULT '0',
  description varchar(2000) NOT NULL DEFAULT '',
  lockToDomain varchar(50) NOT NULL DEFAULT '',
  deleted tinyint(1) unsigned NOT NULL DEFAULT '0',
  TSconfig text,
  subgroup text,
  hide_in_lists tinyint(4) NOT NULL DEFAULT '0',
  workspace_perms tinyint(3) NOT NULL DEFAULT '1',
  category_perms varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (uid),
  KEY parent (pid)
);

INSERT INTO be_groups VALUES
(1, 0, 1490111891, 'redakteure', 'pages_language_overlay:description,pages_language_overlay:hidden,pages_language_overlay:keywords,pages_language_overlay:nav_title,pages_language_overlay:starttime,pages_language_overlay:endtime,pages_language_overlay:doktype,sys_file_metadata:title,sys_file_reference:alternative,sys_file_reference:crop,sys_file_reference:link,sys_file_reference:title,sys_file_collection:hidden,sys_file_collection:sys_language_uid,sys_file_collection:starttime,sys_file_collection:endtime,sys_file_collection:l10n_parent,pages:backend_layout_next_level,pages:backend_layout,pages:description,pages:nav_hide,pages:hidden,pages:keywords,pages:l18n_cfg,pages:nav_title,pages:starttime,pages:endtime,pages:doktype,tt_content:image_zoom,tt_content:imagecols,tt_content:tx_gridelements_children,tt_content:tx_gridelements_container,tt_content:tx_gridelements_columns,tt_content:tx_gridelements_backend_layout,tt_content:imageheight,tt_content:hidden,tt_content:sys_language_uid,tt_content:header_link,tt_content:imageorient,tt_content:starttime,tt_content:endtime,tt_content:l18n_parent,tt_content:header_layout,tt_content:imagewidth', 'tt_content:CType:header:ALLOW,tt_content:CType:textmedia:ALLOW,tt_content:CType:gridelements_pi1:ALLOW', '', NULL, '1', '1,4,3', 'pages,sys_file,sys_file_collection,sys_file_metadata,sys_file_reference,pages_language_overlay,tt_content', 'pages,sys_file,sys_file_collection,sys_file_metadata,sys_file_reference,pages_language_overlay,tt_content', 1490089103, 1, 'web_layout,web_list,file_FilelistList,help_CshmanualCshmanual', '1,2,3', 'readFolder,writeFolder,addFolder,renameFolder,moveFolder,deleteFolder,readFile,writeFile,addFile,renameFile,replaceFile,moveFile,copyFile,deleteFile', 0, '', '', 0, '', '', 0, 1, '');



--
-- Tabellenstruktur für Tabelle be_users
--
DROP TABLE IF EXISTS be_users;
CREATE TABLE be_users (
  uid int(11) unsigned NOT NULL AUTO_INCREMENT,
  pid int(11) unsigned NOT NULL DEFAULT '0',
  tstamp int(11) unsigned NOT NULL DEFAULT '0',
  username varchar(50) NOT NULL DEFAULT '',
  description varchar(2000) NOT NULL DEFAULT '',
  avatar int(11) unsigned NOT NULL DEFAULT '0',
  password varchar(100) NOT NULL DEFAULT '',
  admin tinyint(4) unsigned NOT NULL DEFAULT '0',
  usergroup varchar(255) NOT NULL DEFAULT '',
  disable tinyint(1) unsigned NOT NULL DEFAULT '0',
  starttime int(11) unsigned NOT NULL DEFAULT '0',
  endtime int(11) unsigned NOT NULL DEFAULT '0',
  lang char(2) NOT NULL DEFAULT '',
  email varchar(80) NOT NULL DEFAULT '',
  db_mountpoints text,
  options tinyint(4) unsigned NOT NULL DEFAULT '0',
  crdate int(11) unsigned NOT NULL DEFAULT '0',
  cruser_id int(11) unsigned NOT NULL DEFAULT '0',
  realName varchar(80) NOT NULL DEFAULT '',
  userMods text,
  allowed_languages varchar(255) NOT NULL DEFAULT '',
  uc mediumtext,
  file_mountpoints text,
  file_permissions text,
  workspace_perms tinyint(3) NOT NULL DEFAULT '1',
  lockToDomain varchar(50) NOT NULL DEFAULT '',
  disableIPlock tinyint(1) unsigned NOT NULL DEFAULT '0',
  deleted tinyint(1) unsigned NOT NULL DEFAULT '0',
  TSconfig text,
  lastlogin int(10) unsigned NOT NULL DEFAULT '0',
  createdByAction int(11) NOT NULL DEFAULT '0',
  usergroup_cached_list text,
  workspace_id int(11) NOT NULL DEFAULT '0',
  workspace_preview tinyint(3) NOT NULL DEFAULT '1',
  category_perms varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (uid),
  KEY parent (pid),
  KEY username (username)
);

INSERT INTO be_users VALUES
(1, 0, 1490348269, 'snsesamnet', '', 0, '$P$CgCqdgle49oC8mN1msVANqYx4JxfUJ0', 1, '', 0, 0, 0, 'de', 'info@sesamnet.ch', '', 3, 1490348245, 1, 'sesamnet GmbH', '', '', NULL, '', 'readFolder,writeFolder,addFolder,renameFolder,moveFolder,deleteFolder,readFile,writeFile,addFile,renameFile,replaceFile,moveFile,copyFile,deleteFile', 1, '', 0, 0, '', 0, 0, NULL, 0, 1, ''),
(2, 0, 1490349121, 'gulp', '', 0, '$P$C3NxDb0KnEjJJHExIrMD4vFr.pMxyG1', 0, '1', 0, 0, 0, 'de', '', '', 3, 1490349098, 1, '', '', '', NULL, '1,2,3', 'readFolder,writeFolder,addFolder,renameFolder,moveFolder,deleteFolder,readFile,writeFile,addFile,renameFile,replaceFile,moveFile,copyFile,deleteFile', 1, '', 0, 0, '', 0, 0, NULL, 0, 1, '');


DROP TABLE IF EXISTS pages;
CREATE TABLE pages (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,
	t3_origuid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(1) unsigned DEFAULT '0' NOT NULL,
	perms_userid int(11) unsigned DEFAULT '0' NOT NULL,
	perms_groupid int(11) unsigned DEFAULT '0' NOT NULL,
	perms_user tinyint(4) unsigned DEFAULT '0' NOT NULL,
	perms_group tinyint(4) unsigned DEFAULT '0' NOT NULL,
	perms_everybody tinyint(4) unsigned DEFAULT '0' NOT NULL,
	editlock tinyint(4) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	title varchar(255) DEFAULT '' NOT NULL,
	doktype int(11) unsigned DEFAULT '0' NOT NULL,
	TSconfig text,
	is_siteroot tinyint(4) DEFAULT '0' NOT NULL,
	php_tree_stop tinyint(4) DEFAULT '0' NOT NULL,
	url varchar(255) DEFAULT '' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,
	urltype tinyint(4) unsigned DEFAULT '0' NOT NULL,
	shortcut int(10) unsigned DEFAULT '0' NOT NULL,
	shortcut_mode int(10) unsigned DEFAULT '0' NOT NULL,
	no_cache int(10) unsigned DEFAULT '0' NOT NULL,
	fe_group varchar(100) DEFAULT '0' NOT NULL,
	subtitle varchar(255) DEFAULT '' NOT NULL,
	layout int(11) unsigned DEFAULT '0' NOT NULL,
	target varchar(80) DEFAULT '' NOT NULL,
	media int(11) unsigned DEFAULT '0' NOT NULL,
	lastUpdated int(10) unsigned DEFAULT '0' NOT NULL,
	keywords text,
	cache_timeout int(10) unsigned DEFAULT '0' NOT NULL,
	cache_tags varchar(255) DEFAULT '' NOT NULL,
	newUntil int(10) unsigned DEFAULT '0' NOT NULL,
	description text,
	no_search tinyint(3) unsigned DEFAULT '0' NOT NULL,
	SYS_LASTCHANGED int(10) unsigned DEFAULT '0' NOT NULL,
	abstract text,
	module varchar(255) DEFAULT '' NOT NULL,
	extendToSubpages tinyint(3) unsigned DEFAULT '0' NOT NULL,
	author varchar(255) DEFAULT '' NOT NULL,
	author_email varchar(80) DEFAULT '' NOT NULL,
	nav_title varchar(255) DEFAULT '' NOT NULL,
	nav_hide tinyint(4) DEFAULT '0' NOT NULL,
	content_from_pid int(10) unsigned DEFAULT '0' NOT NULL,
	mount_pid int(10) unsigned DEFAULT '0' NOT NULL,
	mount_pid_ol tinyint(4) DEFAULT '0' NOT NULL,
	alias varchar(32) DEFAULT '' NOT NULL,
	l18n_cfg tinyint(4) DEFAULT '0' NOT NULL,
	fe_login_mode tinyint(4) DEFAULT '0' NOT NULL,
	backend_layout varchar(64) DEFAULT '' NOT NULL,
	backend_layout_next_level varchar(64) DEFAULT '' NOT NULL,
	tsconfig_includes text,
  categories int(11) DEFAULT '0' NOT NULL,
	PRIMARY KEY (uid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY parent (pid,deleted,sorting),
	KEY alias (alias),
	KEY determineSiteRoot (is_siteroot)
);

INSERT INTO pages (pid, sorting, perms_userid, perms_user, perms_group, cruser_id, title, doktype, is_siteroot, urltype, nav_hide)
VALUES (0, 256, 1, 31, 27, 1, 'Root', 1, 1, 1, 0),
(1, 256, 1, 31, 27, 1, '404', 1, 0, 1, 1),
(1, 256, 1, 31, 27, 1, 'Impressum', 1, 0, 1, 1);
