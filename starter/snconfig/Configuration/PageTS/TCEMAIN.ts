# ============================================== #
#                                           _
#                                          | |
#  ___  ___  ___  __ _ _ __ ___  _ __   ___| |_
# / __|/ _ \/ __|/ _` | '_ ` _ \| '_ \ / _ \ __|
# \__ \  __/\__ \ (_| | | | | | | | | |  __/ |_
# |___/\___||___/\__,_|_| |_| |_|_| |_|\___|\__|
#
# Project: 	SNBASE
# Version: 	1.0.0
# Author:	Jan Fässler
# Year:		© 2017
#
# ============================================== #



TCEMAIN {
	# PERMISSIONS
    permissions {
        groupid = 1
        group = show, editcontent, edit, new, delete
        everybody = show
    }

	# TABLES
	table {
		# tt_content
		tt_content {
			disablePrependAtCopy = 1
			disableHideAtCopy = 0
		}

		# pages
		pages {
			disablePrependAtCopy = 1
			disableHideAtCopy = 0
		}
	}
}
