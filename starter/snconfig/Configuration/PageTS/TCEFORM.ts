# ============================================== #
#                                           _
#                                          | |
#  ___  ___  ___  __ _ _ __ ___  _ __   ___| |_
# / __|/ _ \/ __|/ _` | '_ ` _ \| '_ \ / _ \ __|
# \__ \  __/\__ \ (_| | | | | | | | | |  __/ |_
# |___/\___||___/\__,_|_| |_| |_|_| |_|\___|\__|
#
# Project: 	SNBASIC
# Version: 	1.0.0
# Author:	Joel Frehner
# Year:		© 2018
#
# ============================================== #



TCEFORM {
	# PAGES
	pages {
	}

	# TT_CONTENT
	tt_content {
		# REGISTER GENERAL
		# Deactivate Input "Description"
		rowDescription.disabled = 1

		# Deactivate Datepicker "Date"
		date.disabled = 1

		# Header / Title input field
		header_layout {
			removeItems = 0,7,8,9,10,100
			altLabels {
				1 = Überschrift 1
				2 = Überschrift 2
				3 = Überschrift 3
				4 = Überschrift 4
				5 = Überschrift 5
			}
			addItems {
				6 = Überschrift 6
			}
		}
	}
}
