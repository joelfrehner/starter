# ============================================== #
#                                           _
#                                          | |
#  ___  ___  ___  __ _ _ __ ___  _ __   ___| |_
# / __|/ _ \/ __|/ _` | '_ ` _ \| '_ \ / _ \ __|
# \__ \  __/\__ \ (_| | | | | | | | | |  __/ |_
# |___/\___||___/\__,_|_| |_| |_|_| |_|\___|\__|
#
# Project:  snconfig
# Version:  1.0.0
# Author:   Joel Frehner
# Year:     © 2018
#
# ============================================== #



# ========================= #
# EXTENSION SOURCE VARIABLE #
# ========================= #
resDir = EXT:snconfig/Resources
privateDir = {$resDir}/Private
publicDir = {$resDir}/Public
cssDir = {$publicDir}/Css
jsDir = {$publicDir}/JavaScript
iconsDir = {$publicDir}/Icons



# ========================= #
# PAGE VARIABLEN #
# ========================= #
#page {
#	template {
#		starter1LayoutRootPath = {$privateDir}/Layouts/starter1
#		starter1PartialRootPath = {$privateDir}/Partials/starter1
#		starter1TemplateRootPath = {$privateDir}/Templates/starter1
#
#		starter2LayoutRootPath = {$privateDir}/Layouts/starter2
#		starter2PartialRootPath = {$privateDir}/Partials/starter2
#		starter2TemplateRootPath = {$privateDir}/Templates/starter2
#
#		starter3LayoutRootPath = {$privateDir}/Layouts/starter3
#		starter3PartialRootPath = {$privateDir}/Partials/starter3
#		starter3TemplateRootPath = {$privateDir}/Templates/starter3
#	}
#}



# ========================= #
# KONSTANTEN EDITOR #
# ========================= #
# SUBKATEGORIEN
#customsubcategory=100= Konfigurationen
#customsubcategory=110= CSS & JS
#customsubcategory=120= Libraries & Frameworks
#customsubcategory=130= Optionen
#customsubcategory=140= Bilder
#customsubcategory=150= Wichtige Seiten
#customsubcategory=160= Kundeninformationen



# KONSTANTEN
plugin.snconfig {
		# KONFIGURATIONEN
		# cat=snconfig/100/001; type=string; label= Template Extension Pfad: [EXT:sncustom]
		templatesPath =

		# cat=snconfig/100/002; type=boolean; label= E-Mail Spam Schutz: Das mailto wird durch JavaScript ersetzt wenn der Haken gesetzt ist (config.spamProtectEmailAddresses)
		spamProtectEmailAddresses = 1

		# cat=snconfig/100/003; type=string; label= E-Mail Spam Schutz: Das @ Zeichen wird durch die folgende eingabe überschrieben (config.spamProtectEmailAddresses_atSubst)
		spamProtectEmailAddresses_atSubst = (at)

		# cat=snconfig/100/004; type=boolean; label= Admin Panel: Das Admin Panel wird angezeigt wenn der Haken gesetzt ist (config.admPanel)
		admPanel = 1

		# cat=snconfig/100/005; type=boolean; label= Cache Deaktivieren: Der Cache wird deaktiviert wenn der Haken gesetzt ist (config.no_cache)
		no_cache = 0

		# cat=snconfig/100/006; type=boolean; label= Kommentare entfernen: Mehrzeilige und Platzbrauchende Kommentare werden entfernt (config.disablePrefixComment)
		disablePrefixComment = 1

		# cat=snconfig/100/007; type=boolean; label= Development Modus: Der Development Modus wird aktiviert wenn der Haken gesetzt ist (Auswirkung: config.debug=1, config.contentObjectExceptionHandler=0, page.meta.robots=noindex,follow) NACH DER AUFSCHALTUNG DEN HAKEN RAUSNEHMEN!
		developmentMode = 0



		# CSS & JS
		# cat=snconfig/110/001; type=boolean; label= CSS komprimieren: Alle CSS Dateien komprimieren (config.compressCss)
		compressCss = 1

		# cat=snconfig/110/002; type=boolean; label= CSS verbinden: Alle CSS Dateien in einer Datei zusammenführen (config.concatenateCss)
		concatenateCss = 1

		# cat=snconfig/110/003; type=boolean; label= JS komprimieren: Alle JS Dateien komprimieren (config.compressJs)
		compressJs = 1

		# cat=snconfig/110/004; type=boolean; label= JS verbinden: Alle JS Dateien in einer Datei zusammenführen (config.concatenateJs)
		concatenateJs = 1

		# cat=snconfig/110/005; type=boolean; label= JS im Footer inkludieren: Alle JS Datein im Footer inkludieren statt im Head (config.moveJsFromHeaderToFooter)
		moveJsFromHeaderToFooter = 1


		# LIBRARIES & FRAMEWORKS
		# cat=snconfig/120/001; type=string; label= jQuery: Um das jQuery über die snconfig Extension einzubinden einfach die gewünschte Version eingeben, wenn leer wird kein jQuery eingebunden (z.B. 3.2.1)
		jquery = 3.3.1

		# cat=snconfig/120/003; type=string; label= jQuery mmenu: Um das jQuery mmenu über die snconfig Extension einzubinden einfach die gewünschte Version eingeben, wenn leer wird kein jQuery mmenu eingebunden (z.B. 6.1.0)
		mmenu = 7.0.3

		# cat=snconfig/120/005; type=string; label= Font Awesome: Um das Font Awesome über die snconfig Extension einzubinden einfach die gewünschte Version eingeben, wenn leer wird kein Font Awesome eingebunden (z.B. 4.7.0)
		fontawesome = 4.7.0



		# OPTIONEN
		# cat=snconfig/130/001; type=string; label= Google Analytics: Google Analytics Tracking-Code (UA-00000000-0)
		googleanalytics =



		# BILDER
		# cat=snconfig/140/001; type=string; label= Logo: Logo im SVG/PNG Format
		logo = /fileadmin/bilder/design/logo.svg

		# cat=snconfig/140/003; type=string; label= Favicon: Favicon im ICO Format
		favicon = /fileadmin/bilder/design/favicon.ico



		# WICHTIGE SEITEN
		# !PLANNED FOR VERSION 1.1.0
		# # cat=snconfig/150/001; type=int+; label= Error 404 (Seiten ID):
		# error404 = 2

		# cat=snconfig/150/002; type=int+; label= Impressum (Seiten ID):
		impressum = 3

		# cat=snconfig/150/003; type=int+; label= Metanavigation (Seiten ID): Für individuelles Template, nicht im Starter Design
		metalinks =

		# cat=snconfig/150/004; type=int+; label= Footernavigation (Seiten ID): Für individuelles Template, nicht im Starter Design
		footerlinks =



		# KUNDENINFORMATIONEN
		# cat=snconfig/160/001; type=string; label= Firmenname:
		companyname = sesamnet

		# cat=snconfig/160/002; type=options[,einzelfirma,gmbh,ag,kig,verein,stiftung,genossenschaft]; label= Rechtsform: [(empty), Einzelfirma, GmbH, AG, KIG (Kollektivgesellschaft), Verein, Stiftung, Genossenschaft]
		legalform = gmbh

		# cat=snconfig/160/003; type=string; label= Strasse:
		street = Industriestrasse

		# cat=snconfig/160/004; type=string; label= Strassennummer:
		streetnr = 43

		# cat=snconfig/160/005; type=string; label= PLZ:
		plz = 3178

		# cat=snconfig/160/006; type=string; label= Ort:
		city = Bern

		# cat=snconfig/160/007; type=string; label= Nation / Land:
		country = Schweiz

		# cat=snconfig/160/007; type=string; label= Landes Code:
		countryCode = CH

		# cat=snconfig/160/008; type=string; label= Telefon:
		phone = 031 740 70 00

		# cat=snconfig/160/009; type=string; label= Mobile:
		mobile =

		# cat=snconfig/160/010; type=string; label= Fax:
		fax = 031 740 70 90

		# cat=snconfig/160/011; type=string; label= E-Mail:
		email = muster@sesamnet.ch
}
