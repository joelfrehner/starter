# ============================================== #
#                                           _
#                                          | |
#  ___  ___  ___  __ _ _ __ ___  _ __   ___| |_
# / __|/ _ \/ __|/ _` | '_ ` _ \| '_ \ / _ \ __|
# \__ \  __/\__ \ (_| | | | | | | | | |  __/ |_
# |___/\___||___/\__,_|_| |_| |_|_| |_|\___|\__|
#
# Project:  snconfig
# Version:  1.0.0
# Author:   Joel Frehner
# Year:     © 2018
#
# ============================================== #



# ========================== #
# GLOBAL CONFIG SETTINGS #
# ========================== #
config  {
		# URL CONFIGURATION

		#absRefPrefix = /
		tx_realurl_enable = 1
		typolinkCheckRootline = 1
		typolinkEnableLinksAcrossDomains = 1


		# HTML DOCTYPE
		doctype = html5
		xmlprologue = none

		# CHARSET (default)
		metaCharset = utf-8

		# LANGUAGE
		sys_language_uid = 0
		sys_language_overlay = hideNonTranslated
		sys_language_mode = content_fallback
		linkVars = L(0-9)
		language = de
		locale_all = de_DE.UTF-8
		htmlTag_langKey = de

		# REMOVE DEFAULT PAGE TITLE
		noPageTitle = 1

		# MOVE JAVASCRIPTS TO FOOTER
		moveJsFromHeaderToFooter = {$plugin.snconfig.moveJsFromHeaderToFooter}

		# COMPRESS AND MERGE CSS & JS FILES
		compressCss = {$plugin.snconfig.compressCss}
		concatenateCss = {$plugin.snconfig.concatenateCss}
		compressJs = {$plugin.snconfig.compressJs}
		concatenateJs = {$plugin.snconfig.concatenateJs}

		# EMAIL SPAM PROTECTION
		spamProtectEmailAddresses = {$plugin.snconfig.spamProtectEmailAddresses}
		spamProtectEmailAddresses_atSubst = {$plugin.snconfig.spamProtectEmailAddresses_atSubst}

		# REMOVE SPACE CONSUMING COMMENTS
		disablePrefixComment = {$plugin.snconfig.disablePrefixComment}

		# ACTIVE CACHE
		#no_cache = {$plugin.snconfig.no_cache}

		# ADMIN PANEL
		admPanel = {$plugin.snconfig.admPanel}

}

# DEVELOPMENT MODUS
[globalVar = LIT:0<{$plugin.snconfig.developmentMode}]
	config.debug = 1
	config.contentObjectExceptionHandler = 0
	#page.meta.robots = noindex,follow
[else]
	config.debug = 0
	config.contentObjectExceptionHandler = 1
[global]


# ========================== #
# GLOBAL META INFORMATIONS #
# ========================== #
page = PAGE
page {

	# FAVICON
	#shortcutIcon = {$plugin.snconfig.favicon}

	meta {
		# VIEWPORT
		viewport = width=device-width, initial-scale=1

		# PUBLISHER
		publisher = sesamnet GmbH

		# SEO
		description.data = page:description
		keywords.data = page:keywords

		# OPEN GRAPH
		og:site_name.data = getEnv:HTTP_HOST
		og:title.data = TSFE:page|title
		og:description.data = page:description
		og:locale = de_DE
	}
}



# ========================== #
# ALTERNATIV LANGUAGES #
# ========================== #
# IF LANGUAGE UID 1 / FR - Französisch
[globalVar = GP:L = 1]
	config {
		sys_language_uid = 1
		language = fr
		locale_all = fr_FR.utf8
		htmlTag_langKey = fr
	}
	page.meta.og:locale = fr_FR
[global]
# IF LANGUAGE UID 2 / EN - Englisch
[globalVar = GP:L = 2]
	config {
		sys_language_uid = 2
		language = en
		locale_all = en_EN.utf8
		htmlTag_langKey = en
	}
	page.meta.og:locale = en_EN
[global]



#========================== #
#GLOBAL CSS & JS INCLUDES #
#========================== #
# INCLUDE JQUERY IF CONSTANT IS CHECKED
[globalString = LIT:{$plugin.snconfig.jquery}=*.*.*]
	page {
		includeJSFooter {
			#jQuery = https://code.jquery.com/jquery-{$plugin.snconfig.jquery}.min.js
			jQuery = https://cdnjs.cloudflare.com/ajax/libs/jquery/{$plugin.snconfig.jquery}/jquery.min.js
			jQuery.external = 1
			jQuery.forceOnTop = 1
		}
	}
[global]
[globalString = LIT:{$plugin.snconfig.mmenu}=*.*.*]
	page {
		includeJSFooter {
			mmenu = https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/{$plugin.snconfig.mmenu}/jquery.mmenu.all.js
			mmenu.external = 1
		}
		includeCSS {
			mmenu = https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/{$plugin.snconfig.mmenu}/jquery.mmenu.all.css
			mmenu.external = 1
		}
	}
[global]
[globalString = LIT:{$plugin.snconfig.fontawesome}=*.*.*]
	page {
		includeCSS {
			fontAwesome = https://cdnjs.cloudflare.com/ajax/libs/font-awesome/{$plugin.snconfig.fontawesome}/css/font-awesome.css
			fontAwesome.external = 1
		}
	}
[global]

# ========================== #
# SET PAGE TITLE ##
# ========================== #
page {
	headerData {
		99 = TEXT
		99 {
			field = title
			insertData = 1
			noTrimWrap = |<title>|&nbsp;&#124; {$plugin.snconfig.companyname} {LLL:EXT:snconfig/Resources/Private/Language/locallang.xlf:tx_snconfig.{$plugin.snconfig.legalform}}</title>|
		}
	}
}
[globalString = LIT:{$plugin.snconfig.googleanalytics}=UA-*]
	page {
		footerData {
			99 = TEXT
			99 {
				value (
				<script>
					(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
					})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

					ga('create', '{$plugin.snconfig.googleanalytics}', 'auto');
					ga('send', 'pageview');

				</script>
				)
			}
		}
	}
[global]




# ========================== #
# OVERRIDING TT_CONTENT #
# ========================== #
#tt_content.stdWrap >
#tt_content.stdWrap.innerWrap >
#lib.stdheader.stdWrap.dataWrap >
#tt_content.image.20.maxW = 1500
#tt_content.fluidcontent_content.10 >
#lib.parseFunc_RTE.nonTypoTagStdWrap.encapsLines.addAttributes.P.class >



# ========================== #
# INCLUDE GLOBAL TYPOSCRIPTS #
# ========================== #
# TYPOSCRIPT OBJECTS (e.g. customerInformation.txt, rte.txt, stdHeader.txt, txform.txt)
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:snconfig/Configuration/TypoScript/TSContents" extensions="txt">
