
plugin.tx_simpleblog_bloglisting {
  view {
    templateRootPaths.0 = EXT:simpleblog/Resources/Private/Templates/
    templateRootPaths.1 = plugin.tx_simpleblog_bloglisting.view.templateRootPath
    partialRootPaths.0 = EXT:simpleblog/Resources/Private/Partials/
    partialRootPaths.1 = plugin.tx_simpleblog_bloglisting.view.partialRootPath
    layoutRootPaths.0 = EXT:simpleblog/Resources/Private/Layouts/
    layoutRootPaths.1 = plugin.tx_simpleblog_bloglisting.view.layoutRootPath
  }
  persistence {
    #storagePid = plugin.tx_simpleblog_bloglisting.persistence.storagePid
    #recursive = 1
    storagePid = 3,4,5,6,7,9
    #recursive = 1
    classes {
      Pluswerk\Simpleblog\Domain\Model\Blog {
        newRecordStoragePid = 4
      }
      Pluswerk\Simpleblog\Domain\Model\Post {
        newRecordStoragePid = 5
      }
      Pluswerk\Simpleblog\Domain\Model\Comment {
        newRecordStoragePid = 6
      }
      Pluswerk\Simpleblog\Domain\Model\Tag {
        newRecordStoragePid = 7
      }
      Pluswerk\Simpleblog\Domain\Model\Author {
        mapping {
          tableName = fe_users
            columns {
              name.mapOnProperty = fullname
            }
        }
      }
    }
  }
  settings {
    loginpage = 8
  }
  features {
    #skipDefaultArguments = 1
  }
  mvc {
    #callDefaultActionIfActionCantBeResolved = 1
  }
}

page {
  includeCSS {
    bootstrap = EXT:simpleblog/Resources/Public/Bootstrap/css/bootstrap.min.css
    simpleblog = EXT:simpleblog/Resources/Public/Css/simpleblog.css
  }
  includeJSLibs {
    jquery = //code.jquery.com/jquery.js
    jquery.external = 1
    bootstrap = EXT:simpleblog/Resources/Public/Bootstrap/js/bootstrap.min.js
  }
}
