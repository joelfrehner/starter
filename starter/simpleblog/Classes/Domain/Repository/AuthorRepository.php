<?php
/**
 * Created by PhpStorm.
 * User: joel.frehner
 * Date: 21.02.2018
 * Time: 11:00
 */

namespace Pluswerk\Simpleblog\Domain\Repository;


class AuthorRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    protected $defaultOrderings = array('fullname' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING);
}