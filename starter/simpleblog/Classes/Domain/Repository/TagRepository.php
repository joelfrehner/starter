<?php
/**
 * Created by PhpStorm.
 * User: joel.frehner
 * Date: 20.02.2018
 * Time: 14:37
 */

namespace Pluswerk\Simpleblog\Domain\Repository;


class TagRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    protected $defaultOrderings = array('tagvalue' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING);
}