<?php
namespace Pluswerk\Simpleblog\Controller;

/***
 *
 * This file is part of the "Simple Blog Extension" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Joel Frehner &lt;joel.frehner@sesamnet.ch&gt;, Sesamnet
 *
 ***/

/**
 * BlogController
 */
class BlogController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * blogRepository
     *
     * @var \Pluswerk\Simpleblog\Domain\Repository\BlogRepository
     * @inject
     */
    protected $blogRepository = null;

    /**
     * action list
     */
    public function listAction()
    {
        $search = '';
        if ($this->request->hasArgument('search')) {
            $search = $this->request->getArgument('search');
        }

        $limit = ($this->settings['blog']['max']) ?: NULL;

        $blogs = $this->blogRepository->findSearchForm($search, $limit);
        $this->view->assign('blogs',$blogs);
        $this->view->assign('search',$search);
    }

    /**
     * action addForm
     * @param \Pluswerk\Simpleblog\Domain\Model\Blog $blog
     * @return void
     */
    public function addFormAction(\Pluswerk\Simpleblog\Domain\Model\Blog $blog = NULL)
    {
        $this->view->assign('blog',$blog);
    }

    /**
     * action add
     * @param \Pluswerk\Simpleblog\Domain\Model\Blog $blog
     * @validate $blog Pluswerk.Simpleblog:Autocomplete(property=title)
     * @return void
     */
    public function addAction(\Pluswerk\Simpleblog\Domain\Model\Blog $blog)
    {
        $this->blogRepository->add($blog);
        $this->redirect('list');
    }

    /**
     * action show
     * @param \Pluswerk\Simpleblog\Domain\Model\Blog $blog
     * @return void
     */
    public function showAction(\Pluswerk\Simpleblog\Domain\Model\Blog $blog)
    {
        $this->view->assign('blog',$blog);
    }

    /**
     * action updateForm
     * @param \Pluswerk\Simpleblog\Domain\Model\Blog $blog
     * @return void
     */
    public function updateFormAction(\Pluswerk\Simpleblog\Domain\Model\Blog $blog)
    {
        $this->view->assign('blog',$blog);
    }


    /**
     * action update
     * @param \Pluswerk\Simpleblog\Domain\Model\Blog $blog
     * @return void
     */
    public function updateAction(\Pluswerk\Simpleblog\Domain\Model\Blog $blog)
    {
        $this->blogRepository->update($blog);
        $this->redirect('list');
    }


    /**
     * action deleteConfirm
     * @param \Pluswerk\Simpleblog\Domain\Model\Blog $blog
     * @return void
     */
    public function deleteConfirmAction(\Pluswerk\Simpleblog\Domain\Model\Blog $blog)
    {
        $this->view->assign('blog',$blog);
    }

    /**
     * action delete
     * @param \Pluswerk\Simpleblog\Domain\Model\Blog $blog
     * @return void
     */
    public function deleteAction(\Pluswerk\Simpleblog\Domain\Model\Blog $blog)
    {
        $this->blogRepository->remove($blog);
        $this->redirect('list');
    }


}
