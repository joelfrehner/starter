<?php
namespace Pluswerk\Simpleblog\Controller;

/***
 *
 * This file is part of the "Simple Post Extension" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Joel Frehner &lt;joel.frehner@sesamnet.ch&gt;, Sesamnet
 *
 ***/

/**
 * PostController
 */
class PostController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * postRepository
     *
     * @var \Pluswerk\Simpleblog\Domain\Repository\PostRepository
     * @inject
     */
    protected $postRepository = null;

    /**
     * redirect if not logged in
    */
    public function initializeAction()
    {
        $action = $this->request->getControllerActionName();
        // pruefen, ob eine andere Action ausser "show" aufgerufen wurde
        if ($action != 'show') {
            // Redirect zur Login Seite falls nicht eingeloggt
            if (!$GLOBALS['TSFE']->fe_user->user['uid']) {
                $this->redirect(NULL, NULL, NULL, NULL, $this->settings['loginpage']);
            }
        }
    }


    /**
     * action addForm
     * @param \Pluswerk\Simpleblog\Domain\Model\Blog $blog
     * @param \Pluswerk\Simpleblog\Domain\Model\Post $post
     * @return void
     */
    public function addFormAction(\Pluswerk\Simpleblog\Domain\Model\Blog $blog, \Pluswerk\Simpleblog\Domain\Model\Post $post = NULL)
    {
        $this->view->assign('blog',$blog);
        $this->view->assign('post',$post);
        $this->view->assign('tags', $this->objectManager->get('Pluswerk\\Simpleblog\\Domain\\Repository\\TagRepository')->findAll());
    }

    /**
     * action add
     * @param \Pluswerk\Simpleblog\Domain\Model\Blog $blog
     * @param \Pluswerk\Simpleblog\Domain\Model\Post $post
     * @return void
     */
    public function addAction(\Pluswerk\Simpleblog\Domain\Model\Blog $blog, \Pluswerk\Simpleblog\Domain\Model\Post $post)
    {
        $post->setAuthor($this->objectManager->get('Pluswerk\\Simpleblog\\Domain\\Repository\\AuthorRepository')->findOneByUid($GLOBALS['TSFE']->fe_user->user['uid']));
        $blog->addPost($post);
        $this->objectManager->get('Pluswerk\\Simpleblog\\Domain\\Repository\\BlogRepository')->update($blog);
        $this->redirect('show','Blog',NULL,array('blog'=>$blog));
    }

    /**
     * action show
     * @param \Pluswerk\Simpleblog\Domain\Model\Blog $blog
     * @param \Pluswerk\Simpleblog\Domain\Model\Post $post
     * @return void
     */
    public function showAction(\Pluswerk\Simpleblog\Domain\Model\Blog $blog, \Pluswerk\Simpleblog\Domain\Model\Post $post)
    {
        $this->view->assign('blog',$blog);
        $this->view->assign('post',$post);
    }

    /**
     * action updateForm
     * @param \Pluswerk\Simpleblog\Domain\Model\Blog $blog
     * @param \Pluswerk\Simpleblog\Domain\Model\Post $post
     * @return void
     */
    public function updateFormAction(\Pluswerk\Simpleblog\Domain\Model\Blog $blog, \Pluswerk\Simpleblog\Domain\Model\Post $post)
    {
        $this->view->assign('blog',$blog);
        $this->view->assign('post',$post);
        $this->view->assign('tags', $this->objectManager->get('Pluswerk\\Simpleblog\\Domain\\Repository\\TagRepository')->findAll());
    }


    /**
     * action update
     * @param \Pluswerk\Simpleblog\Domain\Model\Blog $blog
     * @param \Pluswerk\Simpleblog\Domain\Model\Post $post
     * @return void
     */
    public function updateAction(\Pluswerk\Simpleblog\Domain\Model\Blog $blog, \Pluswerk\Simpleblog\Domain\Model\Post $post)
    {
        $this->postRepository->update($post);
        $this->redirect('show', 'Blog', NULL, array('blog'=>$blog));
    }


    /**
     * action deleteConfirm
     * @param \Pluswerk\Simpleblog\Domain\Model\Blog $blog
     * @param \Pluswerk\Simpleblog\Domain\Model\Post $post
     * @return void
     */
    public function deleteConfirmAction(\Pluswerk\Simpleblog\Domain\Model\Blog $blog, \Pluswerk\Simpleblog\Domain\Model\Post $post)
    {
        $this->view->assign('blog',$blog);
        $this->view->assign('post',$post);
    }

    /**
     * action delete
     * @param \Pluswerk\Simpleblog\Domain\Model\Blog $blog
     * @param \Pluswerk\Simpleblog\Domain\Model\Post $post
     * @return void
     */
    public function deleteAction(\Pluswerk\Simpleblog\Domain\Model\Blog $blog, \Pluswerk\Simpleblog\Domain\Model\Post $post)
    {
        $blog->removePost($post);
        $this->objectManager->get('Pluswerk\\Simpleblog\\Domain\\Repository\\BlogRepository')->update($blog);
        $this->postRepository->remove($post);
        $this->redirect('show','Blog', NULL, array('blog'=>$blog));
    }


}
