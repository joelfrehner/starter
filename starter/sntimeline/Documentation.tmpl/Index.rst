﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt

.. _start:

=============================================================
Sesamnet Timeline
=============================================================

.. only:: html

	:Classification:
		sntimeline

	:Version:
		|release|

	:Language:
		en

	:Description:
		Typo3 extension to create timelines.

	:Keywords:
		comma,separated,list,of,keywords

	:Copyright:
		2018

	:Author:
		Joel Frehner

	:Email:
		joel.frehner@sesamnet.ch

	:License:
		This document is published under the Open Content License
		available from http://www.opencontent.org/opl.shtml

	:Rendered:
		|today|

	The content of this document is related to TYPO3,
	a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.

	**Table of Contents**

.. toctree::
	:maxdepth: 3
	:titlesonly:

	Introduction/Index
	User/Index
	Administrator/Index
	Configuration/Index
	Developer/Index
	KnownProblems/Index
	ToDoList/Index
	ChangeLog/Index
	Links
