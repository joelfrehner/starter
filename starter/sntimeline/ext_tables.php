<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function($extKey)
    {

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($extKey, 'Configuration/TypoScript', 'Sesamnet Timeline');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_sntimeline_domain_model_timelineevent', 'EXT:sntimeline/Resources/Private/Language/locallang_csh_tx_sntimeline_domain_model_timelineevent.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sntimeline_domain_model_timelineevent');

    },
    $_EXTKEY
);
