<?php
namespace Sntimeline\Sntimeline\Controller;

/***
 *
 * This file is part of the "Sesamnet Timeline" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Joel Frehner &lt;joel.frehner@sesamnet.ch&gt;, Sesamnet
 *
 ***/

/**
 * TimelineEventController
 */
class TimelineEventController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * timelineEventRepository
     *
     * @var \Sntimeline\Sntimeline\Domain\Repository\TimelineEventRepository
     * @inject
     */
    protected $timelineEventRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $timelineEvents = $this->timelineEventRepository->findAll();
        $this->view->assign('timelineEvents', $timelineEvents);
    }
}
