<?php
namespace Sntimeline\Sntimeline\Domain\Repository;

/***
 *
 * This file is part of the "Sesamnet Timeline" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Joel Frehner &lt;joel.frehner@sesamnet.ch&gt;, Sesamnet
 *
 ***/

/**
 * The repository for TimelineEvents
 */
class TimelineEventRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    }
