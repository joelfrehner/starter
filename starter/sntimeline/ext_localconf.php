<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function () {
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Sntimeline.Sntimeline',
            'Pi1',
            [
                'TimelineEvent' => 'list',
            ],
            // non-cacheable actions
            [
                'TimelineEvent' => '',
            ]
        );
    }
);
