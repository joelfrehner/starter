<?php
namespace Sntimeline\Sntimeline\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Joel Frehner <joel.frehner@sesamnet.ch>
 */
class TimelineEventControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Sntimeline\Sntimeline\Controller\TimelineEventController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Sntimeline\Sntimeline\Controller\TimelineEventController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllTimelineEventsFromRepositoryAndAssignsThemToView()
    {

        $allTimelineEvents = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $timelineEventRepository = $this->getMockBuilder(\Sntimeline\Sntimeline\Domain\Repository\TimelineEventRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $timelineEventRepository->expects(self::once())->method('findAll')->will(self::returnValue($allTimelineEvents));
        $this->inject($this->subject, 'timelineEventRepository', $timelineEventRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('timelineEvents', $allTimelineEvents);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }
}
