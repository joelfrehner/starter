# ============================================== #
#                                           _
#                                          | |
#  ___  ___  ___  __ _ _ __ ___  _ __   ___| |_
# / __|/ _ \/ __|/ _` | '_ ` _ \| '_ \ / _ \ __|
# \__ \  __/\__ \ (_| | | | | | | | | |  __/ |_
# |___/\___||___/\__,_|_| |_| |_|_| |_|\___|\__|
#
# Project:  sncustomer
# Version:  1.0.0
# Author:   Joel Frehner
# Year:     © 2018
#
# ============================================== #


# ========================== #
# INCLUDE GLOBAL TYPOSCRIPTS #
# ========================== #
# TYPOSCRIPT OBJECTS (e.g. customerInformation, langNav, legalInfo etc.)
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:sncustomer/Configuration/TypoScript/TSContents" extensions="ts">

// Umlaute und Sonderzeichen werden ersetzt bzw. angepasst
temp.titleSectionId = TEXT
temp.titleSectionId {
    field = title
    trim = 1
    case = lower
    replacement {
        10 {
            search.char = 32
            replace = -
        }
        20 {
            search = /(ä|Ä)/
            useRegExp = 1
            replace = ae
        }
        30 {
            search = /(ö|Ö)/
            useRegExp = 1
            replace = oe
        }
        40 {
            search = /(ü|Ü)/
            useRegExp = 1
            replace = ue
        }
        50 {
            search = ß
            replace = ss
        }
        60 {
            search = /\/\\\.\:\;\,\&/
            useRegExp = 1
            replace =
        }
    }
}

temp.bgColor = TEXT
temp.bgColor {
    field = tx_onepage_color
}


# RENDERING CUSTOM TEMPLATE
page = PAGE
page {
    # Fluidtemplate
    typeNum = 0
    10 = FLUIDTEMPLATE
    10 {
        # Declarate location of Partial and Layouts folder
        #partialRootPath = {$page.template.partialRootPath}
        #layoutRootPath = {$page.template.layoutRootPath}

        file = {$page.template.templateRootPath}/main.html
        #file = fileadmin/dokumente/main.html

        /*
        # Selecting of the TYPO3 Backend Layouts
        file {
            stdWrap {
                cObject = CASE
                cObject {
                    key.data = levelfield:-1, backend_layout, slide
                    key.override.field = backend_layout

                    # Default Layout - Subpage with Subnav
                    default = TEXT
                    default {
                        value = {$page.template.templateRootPath}/subnavLayout.html
                    }

                    # Start/Homepage
                    pagets__homeLayout = TEXT
                    pagets__homeLayout {
                        value = {$page.template.templateRootPath}/homeLayout.html
                        insertData = 1
                    }

                    # Subpage with Subnav
                    pagets__subnavLayout = TEXT
                    pagets__subnavLayout {
                        value = {$page.template.templateRootPath}/subnavLayout.html
                    }

                    # Subpage with Sidebar
                    pagets__sidebarLayout = TEXT
                    pagets__sidebarLayout {
                        value = {$page.template.templateRootPath}/sidebarLayout.html
                    }
                }
            }
        }
        */

        # Custom rendering of TYPO3 Contentelements (selected by row colpos)
        variables {

            # Render Main content
            MAINCONTENT < styles.content.get
            MAINCONTENT {
                select.languageField = sys_language_uid
            }


            NAVIGATION = HMENU
            NAVIGATION {
                wrap = <nav id="main-navigation">|</nav>
                #entryLevel = 0

                1 = TMENU
                1 {
                    #expAll = 1
                    #noBlur = 1
                    wrap = <ul class="nav">|</ul>

                    NO = 1
                    NO {
                        doNotLinkIt = 1
                        linkWrap = <li class="nav-item">|</li>
                        stdWrap >
                        stdWrap {
                            cObject = TEXT
                            cObject {
                                field = nav_title // title
                                typolink {
                                    parameter = 1
                                    section.cObject < temp.titleSectionId
                                    ATagParams = class="nav-link"
                                }
                            }
                        }

                        #stdWrap.htmlSpecialChars = 1
                        #wrapItemAndSub = <li>|</li>
                    }
                }
            }


            page_color = TEXT
            page_color.field = tx_onepage_color

            sectionContent = HMENU
            sectionContent {
                1 = TMENU
                1 {
                    NO = 1
                    NO {
                        doNotLinkIt = 1
                        stdWrap >
                        stdWrap {
                            cObject = COA
                            cObject {
                                if.value = 4
                                if.equals.field = doktype
                                if.negate = 1
                                10 < temp.titleSectionId
                                10.wrap = <section id="|">
                                11 < temp.bgColor
                                11.wrap = <div style="background-color: |">
                                12 = TEXT
                                12 {
                                    if.isTrue.field = tx_pagesaddfields_customcheckbox
                                    wrap = <div class="container">
                                }
                                20 = CONTENT
                                20 {
                                    table = tt_content
                                    select {
                                        pidInList.field = uid
                                    }
                                    wrap = <div class="subpage">|</div>
                                    renderObj < tt_content
                                }
                                30 = TEXT
                                30 {
                                    wrap = </section>
                                }
                            }
                        }
                    }
                }
            }

            page_title = TEXT
            page_title.field = title





        }
    }

    # Include Public files
    includeCSS {
        #mainCss = {$cssDir}/main.css
        bootstrap = https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css
    }
    includeJSFooterlibs {
    }
    includeJSFooter {
        #mainJs = {$jsDir}/main.js
    }
}



