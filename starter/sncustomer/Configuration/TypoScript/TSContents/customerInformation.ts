# ============================================== #
#                                           _
#                                          | |
#  ___  ___  ___  __ _ _ __ ___  _ __   ___| |_
# / __|/ _ \/ __|/ _` | '_ ` _ \| '_ \ / _ \ __|
# \__ \  __/\__ \ (_| | | | | | | | | |  __/ |_
# |___/\___||___/\__,_|_| |_| |_|_| |_|\___|\__|
#
# Project:  SNCUSTOM
# Version:  1.0.0
# Author:   Jan Fässler
# Year:     © 2017
#
# ============================================== #



# CUSTOMERINFORMATIONS
lib.companyName = TEXT
lib.companyName {
	value = {$plugin.snbasic.companyname}
}
lib.legalForm = TEXT
lib.legalForm {
	insertData = 1
	value = {LLL:EXT:snbasic/Resources/Private/Language/locallang.xlf:tx_snbasic.{$plugin.snbasic.legalform}}
}
lib.street = TEXT
lib.street {
	value = {$plugin.snbasic.street}
}
lib.streetNr = TEXT
lib.streetNr {
	value = {$plugin.snbasic.streetnr}
}
lib.country = TEXT
lib.country {
	value = {$plugin.snbasic.country}
}
lib.countryCode = TEXT
lib.countryCode {
	value = {$plugin.snbasic.countryCode}
}
lib.plz = TEXT
lib.plz {
	value = {$plugin.snbasic.plz}
}
lib.city = TEXT
lib.city {
	value = {$plugin.snbasic.city}
}
lib.phone = TEXT
lib.phone {
	value = {$plugin.snbasic.phone}
}
lib.mobile = TEXT
lib.mobile {
	value = {$plugin.snbasic.mobile}
}
lib.fax = TEXT
lib.fax {
	value = {$plugin.snbasic.fax}
}
lib.email = TEXT
lib.email {
	value = {$plugin.snbasic.email}
	typolink.parameter = {$plugin.snbasic.email}
	typolink.parameter.insertData = 1
	typolink.ATagParams = itemprop="email"
}
