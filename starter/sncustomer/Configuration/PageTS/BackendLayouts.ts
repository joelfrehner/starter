# ============================================== #
#                                           _
#                                          | |
#  ___  ___  ___  __ _ _ __ ___  _ __   ___| |_
# / __|/ _ \/ __|/ _` | '_ ` _ \| '_ \ / _ \ __|
# \__ \  __/\__ \ (_| | | | | | | | | |  __/ |_
# |___/\___||___/\__,_|_| |_| |_|_| |_|\___|\__|
#
# Project:  sncustomer
# Version:  1.0.0
# Author:   Jan Fässler
# Year:     © 2017
#
# ============================================== #



# INCLUDE BACKENDLAYOUTS
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:sncustomer/Configuration/PageTS/BackendLayouts" extensions="ts">
