<?php
// ============================================== //
//                                           _
//                                          | |
//  ___  ___  ___  __ _ _ __ ___  _ __   ___| |_
// / __|/ _ \/ __|/ _` | '_ ` _ \| '_ \ / _ \ __|
// \__ \  __/\__ \ (_| | | | | | | | | |  __/ |_
// |___/\___||___/\__,_|_| |_| |_|_| |_|\___|\__|
//
// Project: 	SNBASE
// Version: 	1.0.0
// Author:		Jan Fässler
// Year:		© 2017
//
// ============================================== //



if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}



// ========================== //
// PAGE TS
// ========================== //

// MOD
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/PageTS/BackendLayouts.ts">');