<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'snCustomer',
    'description' => 'Customizable customer plugin. SnBasic has to be included.',
    'category' => 'plugin',
    'author' => 'Joel Frehner',
    'author_email' => 'joel.frehner@sesamnet.ch',
    'state' => 'beta',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-8.7.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
