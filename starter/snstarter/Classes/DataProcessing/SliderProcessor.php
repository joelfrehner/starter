<?php
namespace Sesamnet\snstarter\DataProcessing;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;
use TYPO3\CMS\Frontend\ContentObject\Exception\ContentRenderingException;

class SliderProcessor implements DataProcessorInterface
{
    /**
     * Process data for the CType "slider"
     *
     * @param ContentObjectRenderer $cObj The content object renderer, which contains data of the content element
     * @param array $contentObjectConfiguration The configuration of Content Object
     * @param array $processorConfiguration The configuration of this processor
     * @param array $processedData Key/value store of processed data (e.g. to be passed to a Fluid View)
     * @return array the processed data as key/value store
     * @throws ContentRenderingException
     */
    public function process(ContentObjectRenderer $cObj, array $contentObjectConfiguration, array $processorConfiguration, array $processedData)
    {
        $data = \TYPO3\CMS\Core\Utility\GeneralUtility::xml2array( $cObj->data['pi_flexform'] );
        //$mode = $data['data']['settings']['lDEF']['mode']['vDEF']; --> TYPO3 < 7.6.23

        $image = $data['data']['sheet1']['lDEF']['settings.image']['vDEF'];
        $caption = $data['data']['sheet1']['lDEF']['settings.caption']['vDEF'];
        $description = $data['data']['sheet1']['lDEF']['settings.description']['vDEF'];
        $checkbox = $data['data']['sheet1']['lDEF']['settings.checkbox']['vDEF'];
        $link = $data['data']['sheet1']['lDEF']['settings.link']['vDEF'];
        $test = $data['data']['sheet1']['lDEF']['settings.test']['vDEF'];


        $processedData['slider']['image'] = $image;
        $processedData['slider']['caption'] = $caption;
        $processedData['slider']['description'] = $description;
        $processedData['slider']['checkbox'] = $checkbox;
        $processedData['slider']['link'] = $link;
        $processedData['slider']['test'] = $test;

        return $processedData;
    }
}