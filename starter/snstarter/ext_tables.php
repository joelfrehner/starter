<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function($extKey)
    {

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($extKey, 'Configuration/TypoScript', 'Sesamnet Starter');

    },
    $_EXTKEY
);


/**
 * FlexForms for CType
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue('', 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/slider.xml', 'sn_slider');



/**
 * Add own stylesheet(s) to TYPO3 backend

$GLOBALS['TBE_STYLES']['skins']['snstarter'] = [];
$GLOBALS['TBE_STYLES']['skins']['snstarter']['name'] = 'Slider backend styling';
$GLOBALS['TBE_STYLES']['skins']['snstarter']['stylesheetDirectories'] = [
'EXT:snstarter/Resources/Public/Css/Backend'
];
 *
 */
