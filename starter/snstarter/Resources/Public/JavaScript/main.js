$( document ).ready(function() {
    //MMENU
    var $menu = $("#main-navigation").mmenu({
        //   options
        "extensions": [
            "position-back",
            "position-right"
        ]
    }, {
        // configuration
        clone: true
    });

    //MENU HAMBURGER OPEN/CLOSE
    var $icon = $("#hamburger-icon");
    var API = $menu.data( "mmenu" );

    $icon.on( "click", function() {
        API.open();
    });

    API.bind( "open:finish", function() {
        setTimeout(function() {
            $icon.addClass( "is-active" );
        }, 100);
    });
    API.bind( "close:finish", function() {
        setTimeout(function() {
            $icon.removeClass( "is-active" );
        }, 100);
    });
});