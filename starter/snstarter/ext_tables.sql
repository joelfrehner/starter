#
# Modifying pages table
# adding colorpicker
#
CREATE TABLE pages (
    tx_pagesaddfields_colorpicker varchar(10) DEFAULT ''
);

#
# Modifying pages_language_overlay table
#
CREATE TABLE pages_language_overlay (
    tx_pagesaddfields_colorpicker varchar(10) DEFAULT ''
);
