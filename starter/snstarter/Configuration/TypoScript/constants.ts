# ============================================== #
#                                           _
#                                          | |
#  ___  ___  ___  __ _ _ __ ___  _ __   ___| |_
# / __|/ _ \/ __|/ _` | '_ ` _ \| '_ \ / _ \ __|
# \__ \  __/\__ \ (_| | | | | | | | | |  __/ |_
# |___/\___||___/\__,_|_| |_| |_|_| |_|\___|\__|
#
# Project:  snstarter
# Version:  1.0.0
# Author:   Jan Fässler
# Year:     © 2017
#
# ============================================== #



# ========================= #
# EXTENSION SOURCE VARIABLE #
# ========================= #
resDir = EXT:snstarter/Resources
privateDir = {$resDir}/Private
publicDir = {$resDir}/Public
cssDir = {$publicDir}/Css
jsDir = {$publicDir}/JavaScript
iconsDir = {$publicDir}/Icons



# ========================= #
# PAGE VARIABLEN #
# ========================= #
page {
	template {
		layoutRootPath = {$privateDir}/Layouts
		partialRootPath = {$privateDir}/Partials
		templateRootPath = {$privateDir}/Templates
	}
}



# ========================= #
# KONSTANTEN EDITOR #
# ========================= #
# SUBKATEGORIEN
# customsubcategory=300= Generelle Einstellungen

# KONSTANTEN
plugin.snstarter {


}
