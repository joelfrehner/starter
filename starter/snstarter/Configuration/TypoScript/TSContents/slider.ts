lib.fluidContent {
    templateRootPaths {
        100 = EXT:snstarter/Resources/Private/ContentElements/Slider/Templates
    }
    partialRootPaths {
        100 = EXT:snstarter/Resources/Private/ContentElements/Slider/Partials
    }
    layoutRootPaths {
        100 = EXT:snstarter/Resources/Private/ContentElements/Slider/Layouts
    }

    extbase.controllerExtensionName = snstarter
}


tt_content {
    sn_slider = COA_INT
    sn_slider {
        10 < lib.fluidContent
        10 {
            templateName = Slider
            dataProcessing {
                10 = Sesamnet\snstarter\DataProcessing\SliderProcessor
            }
        }
    }
}