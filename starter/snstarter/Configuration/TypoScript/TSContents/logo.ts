# ============================================== #
#                                           _
#                                          | |
#  ___  ___  ___  __ _ _ __ ___  _ __   ___| |_
# / __|/ _ \/ __|/ _` | '_ ` _ \| '_ \ / _ \ __|
# \__ \  __/\__ \ (_| | | | | | | | | |  __/ |_
# |___/\___||___/\__,_|_| |_| |_|_| |_|\___|\__|
#
# Project:  SNCUSTOM
# Version:  1.0.0
# Author:   Jan Fässler
# Year:     © 2017
#
# ============================================== #

lib.logo = IMAGE
lib.logo {
    file = {$plugin.snconfig.logo}
    imageLinkWrap = 1
    imageLinkWrap {
        enable = 1
        typolink.parameter = 1
    }
}
/*

# LOGO
[globalString = {$plugin.snstarter.svglogo}]
    lib.logo = IMAGE
    lib.logo {
        file = {$plugin.snstarter.svglogo}
        params = onerror="this.src='{$plugin.snstarter.pnglogo}'; this.onerror=null;"
        imageLinkWrap = 1
        imageLinkWrap {
            enable = 1
            typolink.parameter = 1
        }
    }
[else]
    lib.logo = IMAGE
    lib.logo {
        file = {$plugin.snstarter.pnglogo}
        imageLinkWrap = 1
        imageLinkWrap {
            enable = 1
            typolink.parameter = 1
        }
    }
[global]
*/
