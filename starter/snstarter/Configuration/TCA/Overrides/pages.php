<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

// Configure new fields:
$fields = array(
    'tx_pagesaddfields_colorpicker' => array(
        'exclude' => 0,
        'label' => 'LLL:EXT:snstarter/Resources/Private/Language/locallang_db.xlf:pages.tx_pagesaddfields_colorpicker',
        'config' => array(
            'type' => 'input',
            'size' => 10,
            'eval' => 'trim',
            'wizards' => array(
                'colorChoice' => array(
                    'type' => 'colorbox',
                    'title' => 'Color picker',
                    'module' => array(
                        'name' => 'wizard_colorpicker',
                    ),
                    'JSopenParams' => 'height=600,width=380,status=0,menubar=0,scrollbars=1',
                    'exampleImg' => 'EXT:examples/res/images/japanese_garden.jpg',
                )
            )
        )
    ),
);

// Add new fields to pages:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $fields);

// Make fields visible in the TCEforms:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'pages', // Table name
    '--palette--;LLL:EXT:snstarter/Resources/Private/Language/locallang_db.xlf:pages.palette_title;tx_pagesaddfields', // Field list to add
    '1', // List of specific types to add the field list to. (If empty, all type entries are affected)
    'after:nav_title' // Insert fields before (default) or after one, or replace a field
);

// Add the new palette:
$GLOBALS['TCA']['pages']['palettes']['tx_pagesaddfields'] = array(
    'showitem' => 'tx_pagesaddfields_colorpicker'
);