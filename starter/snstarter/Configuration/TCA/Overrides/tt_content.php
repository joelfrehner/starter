<?php
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
    'tt_content',
    'CType',
    [
        'LLL:EXT:snstarter/Resources/Private/Language/locallang_db.xlf:tx_snslider.wizard.title',
        'sn_slider',
        'tx-snslider'
        //'EXT:snstarter/Resources/Public/Icons/logo-freiburghaus-immobilien.jpg'
    ],
    'html',
    'after'
);

$GLOBALS['TCA']['tt_content']['types']['sn_slider'] = [
    'showitem' => '--palette--;Inhaltselement;general, pi_flexform'
];





/* COLORPICKER TT_CONTENT
// Configure new fields:
$temp = array(
    'tx_tt_contentaddfields_colorpicker' => array(
        'exclude' => 0,
        'label' => 'LLL:EXT:snstarter/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt_contentaddfields_colorpicker',
        'config' => array(
            'type' => 'input',
            'size' => 10,
            'eval' => 'trim',
            'wizards' => array(
                'colorChoice' => array(
                    'type' => 'colorbox',
                    'title' => 'Color picker',
                    'module' => array(
                        'name' => 'wizard_colorpicker',
                    ),
                    'JSopenParams' => 'height=600,width=380,status=0,menubar=0,scrollbars=1',
                    #'exampleImg' => 'EXT:examples/res/images/japanese_garden.jpg',
                )
            )
        )
    ),
);


// Add new fields to tt_content:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $temp);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'tt_content',
    'visibility',
    'tx_tt_contentaddfields_colorpicker',
    'after:linkToTop'
);




$temporaryColumn = array(
    'tx_examples_noprint' => array (
        'exclude' => 0,
        'label' => 'LLL:EXT:examples/Resources/Private/Language/locallang_db.xlf:tt_content.tx_examples_noprint',
        'config' => array (
            'type' => 'check',
        )
    )
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    'tt_content',
    $temporaryColumn
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'tt_content',
    'visibility',
    'tx_examples_noprint',
    'after:linkToTop'
);
*/