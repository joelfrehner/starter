TCEFORM {
    pages {
        layout {
            addItems {
                3 = Container
                4 = Container-Fluid
            }
            altLabels {
            }
        }
    }

    tt_content {
        # disable unnecessary options
        rowDescription.disabled = 1
        date.disabled = 1

        layout {
            addItems {
                100 = container
                110 = container-fluid
            }
            altLabels {

            }
        }
    }
}


######################
# CUSTOM NEWS LAYOUT #
######################
tx_news.templateLayouts {
    10 = Newsbox teaser
}