# CUSTOM CONTENTELEMENTS
mod {
    wizards.newContentElement.wizardItems.snstarter {
        header = LLL:EXT:snstarter/Resources/Private/Language/locallang_db.xlf:tx_snslider.tabtitle
        elements {
            sn_slider {
                iconIdentifier = tx-snslider
                title = LLL:EXT:snstarter/Resources/Private/Language/locallang_db.xlf:tx_snslider.wizard.title
                description = LLL:EXT:snstarter/Resources/Private/Language/locallang_db.xlf:tx_snslider.wizard.description
                tt_content_defValues {
                    CType = sn_slider
                }
            }
        }
        show = *
    }
}

# Backend preview
#mod.web_layout.tt_content.preview.freiburghausimmob_teaser = EXT:snstarter/Resources/Private/ContentElements/Teaser/Templates/Preview/Teaser.html
